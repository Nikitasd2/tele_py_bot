from app import db

Model = db.Model
Column = db.Column
String = db.String
Integer = db.Integer

session = db.session


class Users(Model):
    id = Column(Integer, primary_key=True)
    state = Column(String, nullable=True)
    referal_code = Column(String)

    @staticmethod
    def get(id):
        return Users.query.get(id)

    @staticmethod
    def new(id, referal_code, state=''):
        user = Users.get(id)
        if not user:
            new_user = Users(id=id, referal_code=referal_code, state=state)
            session.add(new_user)
            session.commit()
            return new_user

    @staticmethod
    def gs(id):
        user = Users.get(id)
        if user:
            if user.state:
                return user.state
            return ''
        return ''

    @staticmethod
    def us(id, state):
        user = Users.get(id)
        if user:
            user.state = state
            session.add(user)
            session.commit()
            return user

    @staticmethod
    def update_state(id, state):
        return Users.us(id, state)

    @staticmethod
    def get_state(id, state):
        return Users.gs(id, state)


    @staticmethod
    def referal_count(code):
        return Users.query.filter_by(referal_code=code).count()

    @staticmethod
    def all_referal_count():
        return Users.query.filter(Users.referal_code != None).all()


