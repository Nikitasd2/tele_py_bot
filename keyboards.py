from telebot.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, KeyboardButton, InlineKeyboardButton
from data.users.menu import *
from data.users.button_third import *
from data.users.button_second import *
from data.users.button_first import *

start_markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
start_markup.add(*menu)

admin_markup = InlineKeyboardMarkup(row_width=1)
admin_button1 = InlineKeyboardButton(text='Рассылка', callback_data='admin|send')
admin_button2 = InlineKeyboardButton(text='Количество рефералов на ссылке', callback_data='admin|refscount')
admin_button3 = InlineKeyboardButton(text='Количество рефералов', callback_data='admin|refs')
admin_button4 = InlineKeyboardButton(text='Количество заблокированных', callback_data='admin|blocked')
admin_markup.add(*[admin_button1, admin_button2, admin_button3, admin_button4])



