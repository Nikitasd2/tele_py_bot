start_text = '''🤖 Это главное меню бота! 

Бот сотрудничает только с проверенными компаниями! 👍

Внимательно следуйте инструкциям и вы получите перевод нужной вам суммы уже через 5 минут после оформления заявки! 💸

Выберете нужный пункт в меню ниже👇'''

sum_text = '''Вопрос 1 из 3:
💰Сумма:

Какая сумма вам нужна?

Для перехода в раздел, выберите кнопку ниже.

📎Совет: отвечайте честно - ваши ответы не увидят кредиторы.'''

work_text = '''Вопрос 2 из 3:
🤝Работа:

Вы трудоустроены?

Для перехода в раздел, выберите кнопку ниже.'''

delay_text = '''Вопрос 3 из 3:
⚠Просрочки:

Есть ли у вас текущие просрочки?

Для перехода в раздел, выберите кнопку ниже.'''

final_s_text = '''Ну тут типо текст.'''  # Сюда текст обычного займа

final_n_text = '''Ну тут типо текст на 0%.'''  # Сюда текст для займа на 0%


instruction_text = '''lalalal'''  # Сюда текст с инструкцией
