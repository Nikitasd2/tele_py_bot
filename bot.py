from app import bot

from handlers.users.start import *
from handlers.users.menu1 import *
from handlers.users.menu2 import *
from handlers.users.menu3 import *

from handlers.admin.admin_panel import *
from handlers.admin.send import *
from handlers.admin.refscount import *
from handlers.admin.refs import *
from handlers.admin.blocked import *

bot.polling(none_stop=True, timeout=60)