import sys,signal,re
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from telebot import TeleBot


class Config(object):
    SQLALCHEMY_DATABASE_URI = 'sqlite:///main.db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


token = str(sys.argv[1])
signal.alarm(600)

bot = TeleBot(token)

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)

admins_l = [1403998760]

def admins(fn):
    def wrapped(m):
        if m.from_user.id in admins_l:
            return fn(m)
    return wrapped
