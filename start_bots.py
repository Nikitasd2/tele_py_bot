import os

bots = open('data/bot_tokens.txt')
bots = bots.read().split('\n')

for bot in bots:
    if not bot:
        continue

    os.system('python3 bot.py ' + str(bot) + ' > /dev/null &')  # вызываем python скрипт в отдельном потоке
