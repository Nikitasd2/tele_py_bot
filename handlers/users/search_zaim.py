from app import bot
from models import Users
from texts import *
from keyboards import *


@bot.message_handler(func = lambda msg: msg.text in ['Подобрать займ', 'Назад к сумме'])
def search_z(msg):
    Users.us(msg.from_user.id, 'z|1')
    bot.send_message(msg.from_user.id, sum_text, reply_markup=sum_markup)


@bot.message_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'z|1')
def search_z2(msg):
    if msg.text in ['15.000 руб. 💸', '30.000 руб. 💸', '70.000 руб. 💸', '100.000 руб. 💸']:

        Users.us(msg.from_user.id, 'z|2')
        bot.send_message(msg.from_user.id, work_text, reply_markup=works_markup)


@bot.message_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'z|2')
def search_z3(msg):
    if msg.text in ['Да, трудоустроен(а)', 'Нет, не трудоустроен(а)']:
        Users.us(msg.from_user.id, 'z|3')
        bot.send_message(msg.from_user.id, delay_text, reply_markup=delay_markup)


@bot.message_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'z|3')
def search_z_final(msg):
    if msg.text in ['Нет просрочек', 'Да, есть 1', 'Да, есть 2-3', 'Назад к трудоустройству']:

        if msg.text in ['Назад к трудоустройству']:
            Users.us(msg.from_user.id, 'z|2')
            bot.send_message(msg.from_user.id, work_text, reply_markup=works_markup)
            return False

        Users.us(msg.from_user.id, '')
        bot.send_message(msg.from_user.id, final_s_text, reply_markup=start_markup)


