from app import bot
from models import Users
from texts import start_text
from keyboards import start_markup


@bot.message_handler(commands=['start'])
@bot.message_handler(func=lambda msg: msg.text == 'Главное меню')
def start(msg):
    referal_code = msg.text.replace('/start', '').replace(' ', '')
    Users.new(msg.from_user.id, referal_code)
    Users.us(msg.from_user.id, '')
    bot.send_message(msg.from_user.id, start_text, reply_markup=start_markup)
