from app import bot, re
from models import Users
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from data.users.button_third import *
from data.users.menu import *

@bot.message_handler(func = lambda msg: msg.text == menu[2])
def item_receive(msg):

    menu1_1 = InlineKeyboardMarkup(row_width=2)
    admin_button = []
    resItem = 0
    for answer in answers1_items:
        resItem += 1
        admin_button.append(InlineKeyboardButton(text=answer, callback_data='menu_item_' + str(resItem)))

    menu1_1.add(*admin_button)

    Users.us(msg.from_user.id, 'users|menu3_item1')
    bot.send_message(msg.from_user.id, text=title1, reply_markup=menu1_1)

@bot.callback_query_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'users|menu3_item1')
def item2(msg):

    selected_item = msg.data.split('_')[2]

    menu1_2 = InlineKeyboardMarkup(row_width=int(total_row2))
    menu1_2_button = []

    r = re.compile("answer_1_"+str(selected_item))
    newlist = list(filter(r.match, answer2_items))  # Read Note

    for key in newlist:
        if len(key.split('_')) == 5:
            if key.split('_')[4] == 'link':
                menu1_2_button.append(InlineKeyboardButton(text=answer2_items[key],url=answer2_items[key]))
            else:
                continue
        else:
            menu1_2_button.append(InlineKeyboardButton(text=answer2_items[key], callback_data=str(key)))

    menu1_2_button.append(InlineKeyboardButton(text=button_back, callback_data='users|back3.1'))
    menu1_2.add(*menu1_2_button)

    Users.us(msg.from_user.id, 'users|menu3_item2')
    bot.send_message(msg.from_user.id, text=title2, reply_markup=menu1_2)

@bot.callback_query_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'users|menu3_item2')
def item3(msg):

    if msg.data == 'users|back3.1':
        item_receive(msg)
        return False

    Users.us(msg.from_user.id, '')
    bot.send_message(msg.from_user.id, text=title3)