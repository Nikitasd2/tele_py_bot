from app import bot
from models import Users


@bot.callback_query_handler(func = lambda call: call.data == 'admin|send')
def admin_send(msg):
    Users.us(msg.from_user.id, 'admin|send')
    bot.send_message(msg.from_user.id, 'Отправьте текст. Поддерживается HTML разметка.')


@bot.message_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'admin|send')
def admin_send_text(msg):
    users = Users.query.all()
    bot.send_message(msg.from_user.id, 'Рассылка запущена.')
    for u in users:
        try:
            bot.send_message(u.id, msg.text, parse_mode='HTML')
        except:
            pass

    bot.send_message(msg.from_user.id, 'Рассылка выполнена.')
    Users.us(msg.from_user.id, '')