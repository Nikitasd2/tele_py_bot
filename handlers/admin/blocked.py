from app import bot
from models import Users


@bot.callback_query_handler(func = lambda call: call.data == 'admin|blocked')
def admin_blocked(call):
    users = Users.query.all()
    active = 0
    blocked = 0

    bot.send_message(call.from_user.id, 'Проверка началась.')
    for u in users:
        try:
            bot.send_chat_action(u.id, 'typing')
            active += 1
        except:
            blocked += 1
    bot.send_message(call.from_user.id, f'Активных: {active}\nЗаблокированных: {blocked}')