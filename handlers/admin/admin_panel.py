from app import bot, admins
from keyboards import admin_markup
from models import Users


@bot.message_handler(commands=['admin'])
@admins
def admin_manage(msg):
    count = Users.query.count()
    bot.send_message(msg.from_user.id, f'Welcome to admin panel!\nUsers: {count}', reply_markup=admin_markup)
