from app import bot
from models import Users


@bot.callback_query_handler(func=lambda call: call.data == 'admin|refscount')
def admin_refscount(call):
    raw_referal_codes = Users.query.with_entities(Users.referal_code).distinct().all()
    referal_codes = [i[0] for i in raw_referal_codes]
    text = 'Количество рефералов на реферальных ссылках:\n\n'
    for rcode in referal_codes:
        count = Users.query.filter_by(referal_code=rcode).count()
        if rcode not in ['', None]:
            text += f'{rcode}: {count}\n'
        else:
            text += f'Без реферальной ссылки: {count}\n'
    bot.send_message(call.from_user.id, text)
