from app import bot
from models import Users


@bot.callback_query_handler(func = lambda call: call.data == 'admin|refs')
def admin_refs(msg):
    Users.us(msg.from_user.id, 'admin|refs')
    bot.send_message(msg.from_user.id, 'Отправьте код.')


@bot.message_handler(func = lambda msg: Users.gs(msg.from_user.id) == 'admin|refs')
def admin_refs_text(msg):
    rcode = msg.text
    count = Users.query.filter_by(referal_code=rcode).count()

    bot.send_message(msg.from_user.id, f'Количество рефералов на {rcode}: {count}')
    Users.us(msg.from_user.id, '')
